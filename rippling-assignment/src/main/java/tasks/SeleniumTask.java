package tasks;

import java.io.File;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/** TASK 1 [validating Phone and Only Numbers Field in web page]
 * @author jaikant dangi
 *
 */
public class SeleniumTask {

	private static WebDriver driver;
	private static Scanner scanner;
	private static final String REGEX_FOR_ONLY_NUMBERS = ".*[^0-9].*";
	
	public static void main(String[] args) {
		
		scanner = new Scanner(System.in);
		// initiating and launching chrome browser
		String chromeDriverPath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://vanilla-masker.github.io/vanilla-masker/demo.html");
		
		// Executing test case 1
		test_onlyNumbers_field();
		
		// Executing test case 2
		test_phoneNumberField_rendered_as_expected();
		
	}

	
	private static void test_onlyNumbers_field() {
		System.out.println("Test started [test_onlyNumbers_field]");
		System.out.println("Please enter an input");
		
		String input = scanner.nextLine();
		String message = "";
		WebElement onlyNumbersInputBox  = getElementById("numbers");
		int inputValueLength = input.trim().length();
		enterText(onlyNumbersInputBox, input);
		String onlyNumbersInputFieldText = getTextUsingAttribute(onlyNumbersInputBox, "value");
		System.out.println("Value displayed in Only Numbers field on web page:" + onlyNumbersInputFieldText);
		boolean actualOutputOnlyContainsNumbers = !onlyNumbersInputFieldText.matches(REGEX_FOR_ONLY_NUMBERS);
		if (inputValueLength == 1 && input.equals("-")) {
			message = "Hyphen only is displayed on Only Numbers field, Marking it as PASSED test case as it is expected";
		} else { 
			message = actualOutputOnlyContainsNumbers && !onlyNumbersInputFieldText.isEmpty() ?
					  "Test Case : [test_onlyNumbers_field] PASSED" :
					  "Test Case : [test_onlyNumbers_field] FAILED, Please check your input number if it is empty or invalid";
		}
		
		System.out.println(message);
	}
	
	private static void test_phoneNumberField_rendered_as_expected() {
		System.out.println("Test started [test_PhoneNumberField_rendered_as_expected]");
		System.out.println("Please enter an input upto 10 digits.");
		
		String inputPhoneNumber = scanner.nextLine();
		int inputPhoneNumberLength =inputPhoneNumber.trim().length(); 
		WebElement phoneNumberInputBox = getElementById("phone");
		enterText(phoneNumberInputBox, inputPhoneNumber);
		
		// formatting user input as alike as it will be displayed on web page or rendered on web page
		String phoneNumberInputFieldText = getTextUsingAttribute(phoneNumberInputBox, "value");
		System.out.println("Value displayed in Phone field on web page:" + phoneNumberInputFieldText);
		String regexUptoTwoDigits = "(\\d{1,2})";
		String regexForExactTwoDigits = "(\\d{2})";
		String regexForExactFourDigits = "(\\d{4})";
		String regexUptoFourDigits = "(\\d{1,4})";
		String number = "";
		if (inputPhoneNumberLength == 1 && inputPhoneNumber.startsWith("_") || Character.isAlphabetic(inputPhoneNumber.charAt(0))) {
			number = inputPhoneNumber.replaceAll(inputPhoneNumber, "(");
		} else if(!inputPhoneNumber.isEmpty() && inputPhoneNumberLength < 3) {
			number = inputPhoneNumber.replaceFirst(regexUptoTwoDigits, "($1");
		} else if (inputPhoneNumberLength < 7 && inputPhoneNumberLength > 2) {
			number = inputPhoneNumber.replaceFirst(regexForExactTwoDigits + regexUptoFourDigits, "($1) $2");
		} else {
			number = inputPhoneNumber.replaceFirst(regexForExactTwoDigits + regexForExactFourDigits + regexUptoFourDigits, "($1) $2-$3");
		}	
		number = !inputPhoneNumber.matches(REGEX_FOR_ONLY_NUMBERS) || inputPhoneNumberLength ==1  ? number : "";
		String message = phoneNumberInputFieldText.equals(number) ?
						"Test case PASSED : Phone Number rendered as expected on web page" :
						"Test case FAILED : Phone number not rendered as expected on web page OR the value you entered is not valid" + System.getProperty("line.separator") + "[For ex. 'f123' input that starts with a alphabet[Although it will display (12) 3 on web page] or 12345678912 i.e more than 10 digits]";
		System.out.println(message);
	}
	
	
	
	/*****************************************************************
	 * 						Generic Selenium methods
	 *****************************************************************/
	
	
	/** It will return the WebElement using id as a locator.
	 * @param idLocator
	 * @return WebElement
	 */
	private static WebElement getElementById(String idLocator) {
		System.out.println("Getting element using id locator:" + idLocator);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(idLocator)));
		
		return driver.findElement(By.id(idLocator));
	}

	/** It will enter the text into input field by first clearing if any text present and then enter text.
	 * @param element	WebElement
	 * @param textToEnter
	 */
	private static void enterText(WebElement element, String textToEnter) {
		System.out.println("Entering text : " + textToEnter);
		element.clear();
		element.sendKeys(textToEnter);
	}
	
	/** It will get the element text using its attribute.
	 * For e.g attributes can be value/innerText/textContent etc.
	 * @param element	WebElement
	 * @param attribute
	 */
	private static String getTextUsingAttribute(WebElement element, String attribute) {
		System.out.println("Getting attribute  : " + attribute);
		return element.getAttribute(attribute).trim();
	}
		
}
