package tasks;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/** TASK 3
 * @author jaikant dangi
 *
 */
public class SentenceOrdering {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the sentence");
		
		String sentence = sc.nextLine().toLowerCase().trim();
		sc.close();
		
		// getting position of each alphabet and putting in map
		AtomicInteger count = new AtomicInteger();
		Map<Character,Integer> letterPositionMap = new HashMap<>();
		IntStream.range(97, 123).forEach(index -> {
			Character letter = (char)index;
			letterPositionMap.put(letter, count.incrementAndGet());
		});
		
		// calculating sum of each word from sentence and putting in map
		Map<String,Integer> wordAndSumMap = new LinkedHashMap<>();
		String wordArray [] = sentence.split(" ");
		for (String word : wordArray) {
			int positionSum=0;
			for (char letter : word.toCharArray()) {
				positionSum = positionSum + letterPositionMap.get(letter);
			}
			wordAndSumMap.put(word, positionSum);
		}
		
		StringBuilder sortedSentence = new StringBuilder();
		// sorting the word and sum map according to their letter sum
		wordAndSumMap.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue()).forEach(word -> sortedSentence.append(word.getKey()).append(" "));
		
		System.out.println("OUTPUT:" + sortedSentence.substring(0,sortedSentence.lastIndexOf(" ")));
	}

}
