package tasks;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/** Task 2
 * @author jaikant dangi
 *
 */
public class CheckInput {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter TYPE");
		String type = sc.next();
		System.out.println("Enter VALUE to validate.");
		int validationValue = sc.nextInt();
		sc.close();
		// As mentioned in task, inputs are in String and Integer [No decimal values should be provided as input]

		checkInput(type, validationValue);
	}

	/** It will check if input is valid or not based on given table.
	 * @param type HOUR/DAY/WEEK/MONTH/YEAR [case insensitive][if any other types are provided other than these than it will throw Null Pointer Exception]
	 * @param valueToValidate int value [e.g 60,70 etc.]
	 */
	private static void checkInput(String type, int valueToValidate) {
		Map<String,String> typeAndLabel = new HashMap<>();
		typeAndLabel.put("HOUR", "Hourly Wage,10,100");
		typeAndLabel.put("DAY", "Daily Wage,70,600");
		typeAndLabel.put("WEEK", "Weekly Wage,300,3000");
		typeAndLabel.put("MONTH", "Monthly Salary,1200,12000");
		typeAndLabel.put("YEAR", "Annual Salary,14000,140000");
		
		String labelAndValue[]= typeAndLabel.get(type.toUpperCase()).split(",");
		String message = valueToValidate >= Integer.valueOf(labelAndValue[1]) && valueToValidate <= Integer.valueOf(labelAndValue[2]) ? labelAndValue[0] + " is valid" : labelAndValue[0] + " is Invalid"; 
		System.out.println( message);
	}
	

}
